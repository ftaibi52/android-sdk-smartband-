package com.inuker.bluetooth;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.adapter.ProtocolAdapter;
import com.inuker.bluetooth.bean.ConnectBean;
import com.inuker.bluetooth.bean.DataBean;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.utils.BluetoothUtils;
import com.zhuoting.health.bean.ClockInfo;
import com.zhuoting.health.bean.LongSitInfo;
import com.zhuoting.health.notify.IAIDataResponse;
import com.zhuoting.health.util.Crc16Util;
import com.zhuoting.health.util.Tools;
import com.zhuoting.health.write.ProtocolAIWriter;
import com.zhuoting.health.write.ProtocolWriter;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Hqs on 2018/1/16
 */
public class TestProtocolActivity extends AppCompatActivity {

    private SearchResult mResult;
    private Toolbar toolbar;
    private BluetoothDevice mDevice;
    private MyBleService bleService;
    private TextView tvConnectStatus;
    private ProtocolAdapter adapter;
    private RecyclerView protocolList;
    private Button btnConnect;
    private String[] protocolMethods;
    private ProgressDialog progressDialog;
    private boolean isEn = false;
    private byte[] minTemp;
    private byte[] maxTemp;
    private byte[] realTemp;
    private byte[] code;
    private static final int AI = 1001;

    static String aiStr = "-19,-99,-103,-51,-24,-26,-30,-28,-22,-19,-19,-15,-10,-3,2,8,13,15,10,0,-13,-28,-40,-47,-49,-45,-41,-40,-40,-39,-38,-39,-38,-35,-33,-31,-32,-36,-38,-38,-35,-31,-30,-29,-25,-22,-24,-26,-27,-30,-33,-36,-32,-16,17,31,-34,-100,-78,-27,-12,-16,-16,-12,-8,-3,0,2,5,8,15,22,23,21,17,6,-9,-23,-34,-40,-40,-40" +
            ",-41,-41,-39,-37,-36,-34,-33,-34,-34,-34,-36,-33,-29,-29,-27,-23,-22,-21,-19,-18,-20,-22,-25,-27,-26,-20,0,33,19,-63,-99,-52,-9,-8,-15,-12,-5,-1,-1,0,"+
            "3,7,11,17,23,27,26,16,0,-13,-24,-33,-40,-40,-37,-37,-35,-31,-29,-29,-27,-26,-27,-28,-24,-20,-21,-21,-20,-19,-17,-13,-9,-9,-12,-15,-17,-22,-24,-22,-20," +
            "-8,26,41,-23,-90,-70,-22,-8,-14,-15,-8,-2,-2,-2,2,8,14,20,26,29,28,25,16,1,-13,-24,-28,-28,-28,-25,-20,-16,-16,-15,-14,-14,-15,-14,-14,-13,-12,-12,-15" +
            ",-14,-10,-6,-4,-5,-5,-5,-10,-15,-16,-16,-13,-3,26,43,-12,-81,-66,-16,0,-7,-9,-6,-4,-6,-4,0,2,6,13,18,18,14,9,2,-9,-19,-25,-33,-40,-42,-40,-38,-35,-34," +
            "-34,-32,-28,-25,-22,-19,-19,-19,-17,-15,-12,-10,-10,-6,0,3,0,-6,-11,-14,-10,-4,1,25,57,22,-60,-70,-17,11,8,4,6,9,10,13,15,18,24,31,35,39,41,38,26,9,-4" +
            ",-15,-24,-28,-29,-28,-26,-25,-24,-22,-20,-21,-20,-17,-19,-20,-18,-18,-18,-16,-15,-14,-14,-12,-9,-6,-5,-6,-7,-8,-10,-12,-9,15,56,40,-48,-85,-39,1,4,0,3" +
            ",8,10,11,13,15,20,27,33,39,41,36,26,14,-1,-18,-28,-31,-32,-30,-29,-27,-26,-25,-25,-27,-25,-20,-20,-22,-20,-18,-17,-15,-13,-12,-12,-9,-6,-4,-4,-6,-9,-1" +
            "1,-11,-10,-6,16,52,30,-58,-88,-37,1,2,-1,1,4,5,6,11,17,21,25,29,33,36,32,24,13,-1,-15,-24,-27,-28,-28,-27,-27,-28,-29,-29,-30,-29,-26,-23,-21,-20,-19," +
            "-18,-15,-13,-13,-12,-11,-7,-3,-4,-8,-12,-14,-14,-14,-11,9,48,33,-53,-88,-41,0,2,-4,1,8,10,11,13,16,21,27,33,40,45,42,31,16,2,-11,-22,-25,-23,-24,-25,-" +
            "23,-20,-21,-21,-19,-17,-18,-17,-18,-21,-22,-19,-16,-14,-13,-12,-11,-9,-7,-7,-9,-13,-16,-16,-13,-10,4,41,47,-29,-86,-51,-2,7,-1,-1,4,9,10,10,16,24,30,3" +
            "8,44,45,43,38,26,12,0,-10,-17,-19,-17,-16,-16,-16,-14,-12,-13,-15,-14,-10,-10,-13,-14,-10,-8,-8,-9,-7,-4,-3,0,2,1,-1,-6,-7,-5,-8,-6,17,53,25,-58,-77,-" +
            "27,6,4,-1,0,7,11,12,13,20,26,30,34,38,39,38,32,21,4,-11,-20,-22,-22,-20,-15,-10,-8,-9,-12,-10,-7,-6,-6,-3,2,6,3,0,0,2,5,6,9,12,14,12,6,1,0,-2,-2,6,33," +
            "49,1,-61,-54,-12,2,-2,-4,-4,-4,-2,2,5,6,8,8,7,7,9,9,3,-8,-25,-37,-42,-42,-40,-38,-37,-33,-32,-35,-36,-37,-37,-37,-41,-43,-44,-45,-45,-43,-41,-41,-36,-" +
            "19,14,45,56,55,54,54,56,60,62,63,60,49,33,15,2,-6,-9,-10,-9,-8,-5,-5,-9,-9,-7,-8,-9,-9,-8,-6,-6,-7,-5,-5,-5,-2,0,0,-4,-8,-9,-9,-9,-2,26,57,18,-67,-79," +
            "-24,6,4,1,7,10,11,12,13,14,19,26,33,39,41,38,28,11,-6,-16,-19,-19,-19,-19,-19,-17,-17,-17,-17,-15,-13,-14,-16,-15,-12,-9,-9,-8,-7,-5,-2,-1,-6,-13,-17," +
            "-17,-16,-9,19,54,13,-70,-78,-27,0,-2,-4,2,8,9,10,13,16,21,27,33,37,39,36,25,9,-6,-17,-23,-24,-23,-22,-21,-20,-19,-21,-22,-18,-16,-16,-14,-12,-12,-13,-" +
            "13,-10,-7,-4,-2,-1,-2,-7,-13,-16,-15,-11,7,43,34,-47,-90,-49,-7,-1,-6,-3,3,8,11,14,17,22,29,33,35,35,34,27,12,-4,-17,-24,-26,-29,-30,-29,-24,-20,-18,-" +
            "18,-17,-15,-13,-13,-13,-10,-8,-6,-3,0,3,5,7,8,5,0,-3,-4,-2,3,27,62,36,-47,-68,-19,14,11,4,6,11,14,18,22,25,28,32,37,44,48,44,33,18,2,-8,-13,-15,-16,-1" +
            "6,-16,-15,-14,-13,-11,-10,-10,-11,-12,-12,-11,-9,-8,-4,0,2,2,2,0,-3,-5,-8,-10,-10,-6,12,49,49,-26,-76,-41,0,4,0,3,8,11,14,15,17,22,28,34,38,43,45,37,2" +
            "1,5,-8,-16,-19,-19,-19,-16,-14,-13,-12,-10,-9,-6,-5,-5,-6,-7,-6,-5,-4,-2,0,3,5,7,8,7,2,-1,-1,2,4,15,51,70,4,-69,-50,1,18,12,11,16,20,23,24,24,30,35,41" +
            ",50,54,50,43,33,21,9,0,-8,-13,-14,-11,-5,-2,-5,-6,-4,-1,-2,-3,-4,-4,-4,-3,-2,0,0,2,3,2,4,6,3,0,-1,-4,-5,-2,10,46,58,-9,-73,-51,-1,11,5,4,10,15,19,21,2" +
            "3,28,33,37,42,46,45,40,30,14,-1,-11,-17,-18,-16,-14,-13,-10,-9,-9,-8,-6,-4,-3,-3,-4,-4,-3,-3,-2,-1,0,4,7,8,7,4,0,-2,0,0,1,18,58,61,-16,-72,-39,6,14,8," +
            "11,17,18,19,20,22,28,35,39,44,49,49,39,25,11,-2,-12,-17,-17,-15,-12,-13,-13,-11,-10,-9,-8,-7,-5,-6,-9,-9,-6,-4,-5,-5,-3,1,3,4,5,3,-1,-3,-4,-4,0,9,38,6" +
            "5,15,-64,-61,-7,16,11,8,12,15,17,20,23,27,32,38,44,49,49,43,31,15,1,-9,-14,-15,-14,-12,-11,-12,-11,-9,-7,-6,-4,-3,-3,-3,-4,-5,-3,0,0,0,0,1,3,6,8,7,3,-" +
            "3,-5,-4,-2,8,41,65,8,-68,-59,-7,14,8,6,13,19,21,23,25,28,31,36,44,49,49,45,38,21,0,-12,-15,-13,-1"+
            ",-19,-99,-103,-51,-24,-26,-30,-28,-22,-19,-19,-15,-10,-3,2,8,13,15,10,0,-13,-28,-40,-47,-49,-45,-41,-40,-40,-39,-38,-39,-38,-35,-33,-31,-32,-36,-38,-38,-35,-31,-30,-29,-25,-22,-24,-26,-27,-30,-33,-36,-32,-16,17,31,-34,-100,-78,-27,-12,-16,-16,-12,-8,-3,0,2,5,8,15,22,23,21,17,6,-9,-23,-34,-40,-40,-40" +
            ",-41,-41,-39,-37,-36,-34,-33,-34,-34,-34,-36,-33,-29,-29,-27,-23,-22,-21,-19,-18,-20,-22,-25,-27,-26,-20,0,33,19,-63,-99,-52,-9,-8,-15,-12,-5,-1,-1,0,"+
            "3,7,11,17,23,27,26,16,0,-13,-24,-33,-40,-40,-37,-37,-35,-31,-29,-29,-27,-26,-27,-28,-24,-20,-21,-21,-20,-19,-17,-13,-9,-9,-12,-15,-17,-22,-24,-22,-20," +
            "-8,26,41,-23,-90,-70,-22,-8,-14,-15,-8,-2,-2,-2,2,8,14,20,26,29,28,25,16,1,-13,-24,-28,-28,-28,-25,-20,-16,-16,-15,-14,-14,-15,-14,-14,-13,-12,-12,-15" +
            ",-14,-10,-6,-4,-5,-5,-5,-10,-15,-16,-16,-13,-3,26,43,-12,-81,-66,-16,0,-7,-9,-6,-4,-6,-4,0,2,6,13,18,18,14,9,2,-9,-19,-25,-33,-40,-42,-40,-38,-35,-34," +
            "-34,-32,-28,-25,-22,-19,-19,-19,-17,-15,-12,-10,-10,-6,0,3,0,-6,-11,-14,-10,-4,1,25,57,22,-60,-70,-17,11,8,4,6,9,10,13,15,18,24,31,35,39,41,38,26,9,-4" +
            ",-15,-24,-28,-29,-28,-26,-25,-24,-22,-20,-21,-20,-17,-19,-20,-18,-18,-18,-16,-15,-14,-14,-12,-9,-6,-5,-6,-7,-8,-10,-12,-9,15,56,40,-48,-85,-39,1,4,0,3" +
            ",8,10,11,13,15,20,27,33,39,41,36,26,14,-1,-18,-28,-31,-32,-30,-29,-27,-26,-25,-25,-27,-25,-20,-20,-22,-20,-18,-17,-15,-13,-12,-12,-9,-6,-4,-4,-6,-9,-1" +
            "1,-11,-10,-6,16,52,30,-58,-88,-37,1,2,-1,1,4,5,6,11,17,21,25,29,33,36,32,24,13,-1,-15,-24,-27,-28,-28,-27,-27,-28,-29,-29,-30,-29,-26,-23,-21,-20,-19," +
            "-18,-15,-13,-13,-12,-11,-7,-3,-4,-8,-12,-14,-14,-14,-11,9,48,33,-53,-88,-41,0,2,-4,1,8,10,11,13,16,21,27,33,40,45,42,31,16,2,-11,-22,-25,-23,-24,-25,-" +
            "23,-20,-21,-21,-19,-17,-18,-17,-18,-21,-22,-19,-16,-14,-13,-12,-11,-9,-7,-7,-9,-13,-16,-16,-13,-10,4,41,47,-29,-86,-51,-2,7,-1,-1,4,9,10,10,16,24,30,3" +
            "8,44,45,43,38,26,12,0,-10,-17,-19,-17,-16,-16,-16,-14,-12,-13,-15,-14,-10,-10,-13,-14,-10,-8,-8,-9,-7,-4,-3,0,2,1,-1,-6,-7,-5,-8,-6,17,53,25,-58,-77,-" +
            "27,6,4,-1,0,7,11,12,13,20,26,30,34,38,39,38,32,21,4,-11,-20,-22,-22,-20,-15,-10,-8,-9,-12,-10,-7,-6,-6,-3,2,6,3,0,0,2,5,6,9,12,14,12,6,1,0,-2,-2,6,33," +
            "49,1,-61,-54,-12,2,-2,-4,-4,-4,-2,2,5,6,8,8,7,7,9,9,3,-8,-25,-37,-42,-42,-40,-38,-37,-33,-32,-35,-36,-37,-37,-37,-41,-43,-44,-45,-45,-43,-41,-41,-36,-" +
            "19,14,45,56,55,54,54,56,60,62,63,60,49,33,15,2,-6,-9,-10,-9,-8,-5,-5,-9,-9,-7,-8,-9,-9,-8,-6,-6,-7,-5,-5,-5,-2,0,0,-4,-8,-9,-9,-9,-2,26,57,18,-67,-79," +
            "-24,6,4,1,7,10,11,12,13,14,19,26,33,39,41,38,28,11,-6,-16,-19,-19,-19,-19,-19,-17,-17,-17,-17,-15,-13,-14,-16,-15,-12,-9,-9,-8,-7,-5,-2,-1,-6,-13,-17," +
            "-17,-16,-9,19,54,13,-70,-78,-27,0,-2,-4,2,8,9,10,13,16,21,27,33,37,39,36,25,9,-6,-17,-23,-24,-23,-22,-21,-20,-19,-21,-22,-18,-16,-16,-14,-12,-12,-13,-" +
            "13,-10,-7,-4,-2,-1,-2,-7,-13,-16,-15,-11,7,43,34,-47,-90,-49,-7,-1,-6,-3,3,8,11,14,17,22,29,33,35,35,34,27,12,-4,-17,-24,-26,-29,-30,-29,-24,-20,-18,-" +
            "18,-17,-15,-13,-13,-13,-10,-8,-6,-3,0,3,5,7,8,5,0,-3,-4,-2,3,27,62,36,-47,-68,-19,14,11,4,6,11,14,18,22,25,28,32,37,44,48,44,33,18,2,-8,-13,-15,-16,-1" +
            "6,-16,-15,-14,-13,-11,-10,-10,-11,-12,-12,-11,-9,-8,-4,0,2,2,2,0,-3,-5,-8,-10,-10,-6,12,49,49,-26,-76,-41,0,4,0,3,8,11,14,15,17,22,28,34,38,43,45,37,2" +
            "1,5,-8,-16,-19,-19,-19,-16,-14,-13,-12,-10,-9,-6,-5,-5,-6,-7,-6,-5,-4,-2,0,3,5,7,8,7,2,-1,-1,2,4,15,51,70,4,-69,-50,1,18,12,11,16,20,23,24,24,30,35,41" +
            ",50,54,50,43,33,21,9,0,-8,-13,-14,-11,-5,-2,-5,-6,-4,-1,-2,-3,-4,-4,-4,-3,-2,0,0,2,3,2,4,6,3,0,-1,-4,-5,-2,10,46,58,-9,-73,-51,-1,11,5,4,10,15,19,21,2" +
            "3,28,33,37,42,46,45,40,30,14,-1,-11,-17,-18,-16,-14,-13,-10,-9,-9,-8,-6,-4,-3,-3,-4,-4,-3,-3,-2,-1,0,4,7,8,7,4,0,-2,0,0,1,18,58,61,-16,-72,-39,6,14,8," +
            "11,17,18,19,20,22,28,35,39,44,49,49,39,25,11,-2,-12,-17,-17,-15,-12,-13,-13,-11,-10,-9,-8,-7,-5,-6,-9,-9,-6,-4,-5,-5,-3,1,3,4,5,3,-1,-3,-4,-4,0,9,38,6" +
            "5,15,-64,-61,-7,16,11,8,12,15,17,20,23,27,32,38,44,49,49,43,31,15,1,-9,-14,-15,-14,-12,-11,-12,-11,-9,-7,-6,-4,-3,-3,-3,-4,-5,-3,0,0,0,0,1,3,6,8,7,3,-" +
            "3,-5,-4,-2,8,41,65,8,-68,-59,-7,14,8,6,13,19,21,23,25,28,31,36,44,49,49,45,38,21,0,-12,-15,-13,-1";


    Handler handler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case AI:
                    String str = msg.obj.toString();
                    if (str == null){
                    }else{
                        try {
                            JSONObject json = new JSONObject(str);
                            int error = json.getInt("code");
                            if (error == 200){
                                String url = json.getJSONObject("data").getString("ViewUrl");
                                Intent intent = new Intent(TestProtocolActivity.this,AIWebviewActivity.class);
                                intent.putExtra("url", url);
                                startActivity(intent);
                            }else{
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_protocol);

        ProtocolAIWriter.newInstance().setIaiDataResponse(iaiDataResponse);

        List<String> methodNames = new ArrayList<>();
        Method[] methods = ProtocolWriter.class.getMethods();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < methods.length; i++) {
            stringBuilder.append("\"");
            stringBuilder.append(methods[i].getName());
            stringBuilder.append("\",");
        }

        protocolMethods = new String[]{

                "时间设置(syncTime)",
                "设置闹钟",
                "删除指定闹钟",
                "修改指定闹钟",
                "查询闹钟",
                "设置目标",
                "用户信息设置",
                "单位设置",
                "久坐提醒设置",
                "防丢提醒设置",
                "防丢提醒参数设置",
                "左右手佩戴设置",
                "手机系统设置",
                "通知设置",
                "心率报警提醒",
                "心率监测模式设置",
                "寻找手机设置",
                "恢复出厂设置",
                "勿扰模式设置",
                "语言设置",
                "抬腕亮屏设置",
                "显示屏亮度设置",
                "肤色设置",
                "血压范围设置",

                "获取基本信息(getBaseInfo)",
                "获取MAC地址",
                "获取设备名字和型号",
                "获取当前心率",
                "获取当前血压",
                "获取手环支持的功能列表",

                "心率测试开关控制",
                "血压测试开关测试",
                "APP退出",
                "信息提醒命令",
                "实时数据上传控制",
                "波形上传控制",
                "天气预报推送",

                "寻找手环",

                "同步历史血压数据",
                "同步历史心率数据",
                "同步历史睡眠数据",
                "同步历史运动数据",
                "删除血压数据",
                "删除心率数据",
                "删除睡眠数据",
                "删除运动数据",
                "writeForAppResponseBlock(Block确认)",
                "运动模式启动与停止",
                "查询采样率",
                "AI诊断"
        };

        Log.e("methods", stringBuilder.toString());
        for (Method method : methods) {
            String name = method.getName();
            methodNames.add(name);
        }

        adapter = new ProtocolAdapter(this, protocolMethods);

        Intent intent = getIntent();
        String mac = intent.getStringExtra("mac");
        mResult = intent.getParcelableExtra("device");
        bleService = MyBleService.getInstance();
        mDevice = BluetoothUtils.getRemoteDevice(mac);

        findView();
        connectDeviceIfNeeded();
        EventBus.getDefault().register(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(true);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBleConnectStateChange(ConnectBean connectBean) {
        Log.e("eventBus", connectBean.isConnect + "");
        if (connectBean.isConnect) {
            tvConnectStatus.setText("已连接");
            tvConnectStatus.setTextColor(Color.BLUE);
            btnConnect.setEnabled(false);
            //bleService.write(ProtocolWriter.writeForDataUpload((byte) 1, (byte) 0, (byte) 2));
        } else {
            tvConnectStatus.setText("未连接");
            tvConnectStatus.setTextColor(Color.BLACK);
            btnConnect.setEnabled(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceiver(DataBean dataBean) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        if (dataBean.data.length() > 50) {
            String receData = dataBean.data.substring(0, 50);
        }
        Toast.makeText(this, dataBean.data ,Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void findView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClientManager.getClient().disconnect(mDevice.getAddress());
                finish();
            }
        });
        tvConnectStatus = (TextView) findViewById(R.id.tv_connect_status);
        protocolList = (RecyclerView) findViewById(R.id.recyclerView);
        protocolList.setLayoutManager(new LinearLayoutManager(this));
        protocolList.setAdapter(adapter);
        Log.e("test", "setAdapter");


        adapter.setOnButtonClickListener(new ProtocolAdapter.OnButtonClickListener() {
            @Override
            public void onButtonClick(View view, int position) {
                Log.e("click", "click position : " + position);
                if (bleService.isConnected) {
                    progressDialog.show();
                    switch (position) {
                        //手环时间设置
                        case 0:
                            bleService.write(ProtocolWriter.syncTime());
                            break;
                        //设置闹钟
                        case 1:
                            ClockInfo clockInfo = new ClockInfo();
                            clockInfo.c_hour = 20;
                            clockInfo.c_min = 22;
                            clockInfo.type = 0;
                            clockInfo.uplater = 20;
                            clockInfo.t_open = true;
                            //第一位开关，后面7位，周一至周日重复
                            clockInfo.valueArray = "1,2,3,5,7";

                            // 起床，10点10分 周一至周五重复，懒睡20分钟
                            bleService.write(ProtocolWriter.writeForAlarmSetting(clockInfo));
                            break;
                        //删除闹钟
                        case 2:
                            // done
                            ClockInfo clockInfo2 = new ClockInfo();
                            clockInfo2.c_hour = 20;
                            clockInfo2.c_min = 21;
                            clockInfo2.type = 0;
                            clockInfo2.uplater = 20;
                            clockInfo2.t_open = true;
                            //第一位开关，后面7位，周一至周日重复
                            clockInfo2.valueArray = "1,2,3,5,7";

                            bleService.write(ProtocolWriter.writeForDeleteAlarm(clockInfo2));
                            break;

                        //修改闹钟
                        case 3:
                            ClockInfo oclockInfo = new ClockInfo();
                            oclockInfo.c_hour = 20;
                            oclockInfo.c_min = 21;
                            oclockInfo.type = 0;
                            oclockInfo.uplater = 20;
                            //第一位开关，后面7位，周一至周日重复
                            oclockInfo.t_open = true;
                            oclockInfo.valueArray = "1,2,3,5,7";


                            // done
                            ClockInfo nclockInfo = new ClockInfo();
                            nclockInfo.c_hour = 20;
                            nclockInfo.c_min = 25;
                            nclockInfo.type = 0;
                            nclockInfo.uplater = 0;
                            nclockInfo.t_open = true;
                            //第一位开关，后面7位，周一至周日重复
                            nclockInfo.valueArray = "1,3,4,5,6,7";

                            bleService.write(ProtocolWriter.writeForModifyAlarm(oclockInfo, nclockInfo));
                            break;
                        //查询闹钟
                        case 4:
                            bleService.write(ProtocolWriter.writeForQueryAlarmClock());
                            break;
                        //设置目标
                        case 5:
                            bleService.write(ProtocolWriter.writeForTargetSetting((byte) 0, 12000, (byte) 8, (byte) 10));
                            break;
                        //用户信息设置
                        case 6:
                            bleService.write(ProtocolWriter.writeForUserInfoSetting(170, 67, (byte) 1, (byte) 26));
                            break;
                        //单位设置
                        case 7:
                            bleService.write(ProtocolWriter.writeForSettingUnit((byte) 0, (byte) 2, (byte) 0, (byte) 0));
                            break;
                        //久坐提醒设置
                        case 8:
                            LongSitInfo longSitInfo = new LongSitInfo();
                            longSitInfo.s_hour1 = 10;
                            longSitInfo.s_min1 = 10;
                            longSitInfo.e_hour1 = 10;
                            longSitInfo.e_min1 = 20;

                            longSitInfo.s_hour2 = 11;
                            longSitInfo.s_min2 = 10;
                            longSitInfo.e_hour2 = 11;
                            longSitInfo.e_min2 = 20;

                            longSitInfo.remindGap = 1;//1,15分钟，2，30分钟，3，45分钟，4，60分钟
                            longSitInfo.open = true;
                            longSitInfo.valueArray = "1,2,3";//周一至周日的值

                            bleService.write(ProtocolWriter.writeForLongSitSetting(longSitInfo));
                            break;
                        //防丢提醒设置
                        case 9:
                            bleService.write(ProtocolWriter.writeForPreventLostSetting((byte) 2));
                            break;

                        //防丢提醒参数设置
                        case 10:
                            bleService.write(ProtocolWriter.writeForPreventLostParamSetting((byte) 2, (byte) 30, (byte) 20, (byte) 1, (byte) 1));
                            break;

                        //左右手佩戴设置
                        case 11:
                            bleService.write(ProtocolWriter.writeForLeftOrRightHand((byte) 0));
                            break;

                        //手机系统设置
                        case 12:
                            bleService.write(ProtocolWriter.writeForMobileOSSetting((byte) 0, (byte) 21));
                            break;

                        //通知设置
                        case 13:
                            bleService.write(ProtocolWriter.writeForNotification((byte) 1, (byte) 0b11111111, (byte) 0b11111111));
                            break;

                        //心率报警设置
                        case 14:
                            bleService.write(ProtocolWriter.writeForHeartRateAlarm((byte) 1, 120));
                            break;

                        //心率监测模式设置
                        case 15:
                            bleService.write(ProtocolWriter.writeForHeartRateMonitor((byte) 1, (byte) 10));
                            break;

                        //寻找手机设置
                        case 16:
                            bleService.write(ProtocolWriter.writeForFindPhoneSetting((byte) 1));
                            break;
                        //恢复出厂设置
                        case 17:
                            bleService.write(ProtocolWriter.writeForRecoverToDefault());
                            break;
                        //勿扰模式设置
                        case 18:
                            bleService.write(ProtocolWriter.writeForDonotDisturbe((byte) 1, (byte) 10, (byte) 40, (byte) 11, (byte) 20));
                            break;

                        //语言设置
                        case 19:
                            if (isEn) {
                                bleService.write(ProtocolWriter.writeForLauguageSetting((byte) 1));
                            } else {
                                bleService.write(ProtocolWriter.writeForLauguageSetting((byte) 0));
                            }
                            isEn = !isEn;
                            break;
                        //抬腕亮屏设置
                        case 20:
                            bleService.write(ProtocolWriter.writeForLeftTheWristToBright((byte) 1));
                            break;

                        //显示屏亮度设置
                        case 21:
                            bleService.write(ProtocolWriter.writeForBrightnessSetting((byte) 1));
                            break;

                        //肤色设置
                        case 22:
                            bleService.write(ProtocolWriter.setSkinColor((byte) 0));
                            break;

                        //血压范围设置
                        case 23:
                            bleService.write(ProtocolWriter.writeForBloodPressure((byte) 1));
                            break;
                        //获取基本信息
                        case 24:
                            bleService.write(ProtocolWriter.getBaseInfo());
                            break;

                        //获取MAC地址
                        case 25:
                            bleService.write(ProtocolWriter.writeForGetMac());
                            break;
                        //获取设备名字和型号
                        case 26:
                            bleService.write(ProtocolWriter.writeForGetDeviceNameOrVersion());
                            break;

                        //获取当前心率
                        case 27:
                            bleService.write(ProtocolWriter.writeForGetCurrentHeartRate());
                            break;

                        //获取当前血压
                        case 28:
                            bleService.write(ProtocolWriter.writeForGetCurrentBP());
                            break;

                        //获取手环支持功能列表

                        case 29:
                            bleService.write(ProtocolWriter.writeForGetSupportFunction());
                            break;
                        //心率测试开关控制

                        case 30:
                            bleService.write(ProtocolWriter.writeForHRMeasure((byte) 2));
                            break;

                        //血压测试开关控制
                        case 31:
                            bleService.write(ProtocolWriter.writeForBPMeasure((byte) 2));
                            break;

                        //app退出指令
                        case 32:
                            bleService.write(ProtocolWriter.writeForAppExit());
                            break;

                        //信息提醒命令

                        case 33:
                            String a1 = "我们\0";
                            String b1 = "测试";
                            try {
                                byte[] l = a1.getBytes("utf-8");
                                byte[] ll = b1.getBytes("utf-8");
                                bleService.write(ProtocolWriter.writeForInformationNotification((byte) 3, l, ll));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            break;


                        //实时数据上传控制
                        case 34:
                            bleService.write(ProtocolWriter.writeForDataUpload((byte) 1, (byte) 1, (byte) 2));
                            break;

                        //波形上传控制
                        case 35:
                            bleService.write(ProtocolWriter.writeForWaveUploadControl((byte) 1, (byte) 1));
                            break;

                        //天气预报推送
                        case 36:

                            bleService.write(ProtocolWriter.writeForWeather("16", "25", "22", 1));
                            break;

                        //寻找手环
                        case 37:
                            bleService.write(ProtocolWriter.writeForFindBand((byte) 1, (byte) 5, (byte) 3));
                            break;

                        //同步历史血压数据
                        case 38:
                            bleService.write(ProtocolWriter.writeForSychronHistoryBPData((byte) 1));
                            break;
                        //同步历史心率数据
                        case 39:
                            bleService.write(ProtocolWriter.writeForSychronHistoryHRData((byte) 1));
                            break;
                        //同步历史睡眠数据
                        case 40:
                            bleService.write(ProtocolWriter.writeForSychronHistorySleepData((byte) 1));
                            break;
                        //同步历史运动数据
                        case 41:
                            bleService.write(ProtocolWriter.writeForSychronHistorySportData((byte) 1));
                            break;
                        //删除历史血压数据
                        case 42:
                            bleService.write(ProtocolWriter.writeForDeleteBPData((byte) 2));
                            break;
                        //删除历史心率数据
                        case 43:
                            bleService.write(ProtocolWriter.writeForDeleteHRData((byte) 2));
                            break;
                        //删除历史睡眠数据
                        case 44:
                            bleService.write(ProtocolWriter.writeForDeleteSleepData((byte) 2));
                            break;
                        //删除历史运动数据
                        case 45:
                            bleService.write(ProtocolWriter.writeForDeleteSportData((byte) 2));
                            break;
                        //Block
                        case 46:
                            //  done
                            bleService.write(ProtocolWriter.writeForAppResponseBlock((byte) 0));
                            break;


                        //运动模式启动、停止
                        case 47:
                            bleService.write(ProtocolWriter.setSportType((byte) 1, (byte) 1));
                            break;
                        case 48:
                            bleService.write(ProtocolWriter.writeForQuerySamplingFreq((byte)1));
                            break;
                        case 49:
                            ProtocolAIWriter.newInstance().writeForAI(NumberUtils.getList(aiStr),SPUtils.readBleAddress(TestProtocolActivity.this),18,0,"13607922537",86,120,74);
                            break;
                    }
                }
            }
        });

        /**
         * protocolMethods = new String[]{"getBaseInfo","syncTime","unBindDevice","writeForAerobicCoachOnOff",
         "writeForAlarmSetting","writeForAllDataOnOff","writeForAppExit",
         "writeForAppResponseBlock","writeForBPMeasure","writeForBrightnessSetting","writeForCorrectBP",
         "writeForDataUpload","writeForDeleteAlarm","writeForDeleteBPData","writeForDeleteHRData",
         "writeForDeleteSleepData","writeForDeleteSportData","writeForDonotDisturbe","writeForFindBand",
         "writeForFindPhone","writeForFindPhoneSetting","writeForGetCurrentBP","writeForGetCurrentHeartRate",
         "writeForGetDeviceNameOrVersion","writeForGetDeviceNotificationStatus","writeForGetMac",
         "writeForGetSupportFunction","writeForHRMeasure","writeForHeartRateAlarm","writeForHeartRateMonitor",
         "writeForLauguageSetting","writeForLeftOrRightHand","writeForLeftTheWristToBright","writeForLongSitSetting",
         "writeForMobileOSSetting","writeForModifyAlarm","writeForNotification",
         "writeForPreventLost","writeForPreventLostParamSetting","writeForPreventLostSetting",
         "writeForQueryAlarmClock","writeForQuerySamplingFreq","writeForRecoverToDefault",
         "writeForSettingUnit","writeForSychronHistoryBPData","writeForSychronHistoryHRData",
         "writeForSychronHistorySleepData","writeForSychronHistorySportData","writeForSychronTodayBPData",
         "writeForSychronTodayHRData","writeForSychronTodaySleepData","writeForSychronTodaySportData",
         "writeForTargetSetting","writeForUserInfoSetting","writeForWaveUploadControl",};
         */


        btnConnect = (Button) findViewById(R.id.btn_connect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectDevice();
            }
        });
    }


    private void connectDeviceIfNeeded() {
        if (!bleService.isConnected) {
            connectDevice();
        }
    }

    private void connectDevice() {
        bleService.connect(mDevice.getAddress());
    }


    IAIDataResponse iaiDataResponse = new IAIDataResponse() {
        @Override
        public void onAIUrlResponse(String data) {
              Log.e("uuuuu",data);
            Message msg = new Message();
            msg.obj = data;
            msg.what = AI;
            handler.sendMessage(msg);
        }
    };
}
