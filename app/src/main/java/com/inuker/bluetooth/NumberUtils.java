package com.inuker.bluetooth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NumberUtils {
    static List<Integer> list = new ArrayList<>();

    public static List<Integer> getList(String aiStringList) {
        aiStringList = aiStringList.trim();
        List<String> aiList = Arrays.asList(aiStringList.split(","));
        for (String str : aiList) {
            int i = Integer.parseInt(str);
            list.add(i);
        }
        return list;
    }
}
