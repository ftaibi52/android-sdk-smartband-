package com.inuker.bluetooth;

import android.content.Context;
import android.content.SharedPreferences;

public class SPUtils {

    //=====
    public static void saveBleAddress(String value,Context context){
        //value 1开始
        SharedPreferences sp = context.getSharedPreferences("smartam", context.MODE_PRIVATE);
        //存入数据
        SharedPreferences.Editor editor = sp.edit();
        if (value == null){
            editor.remove("saveBleAddress");
        }else{
            editor.putString("saveBleAddress", value);
        }
        editor.commit();
    }
    public static String readBleAddress(Context context){
        //value 1开始
        SharedPreferences sp = context.getSharedPreferences("smartam", context.MODE_PRIVATE);
        String value = sp.getString("saveBleAddress",null);
        return value;
    }
}
