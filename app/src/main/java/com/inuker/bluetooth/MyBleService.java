package com.inuker.bluetooth;

import android.app.Service;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telecom.Call;
import android.util.Log;
import android.widget.Toast;

import com.inuker.bluetooth.bean.ConnectBean;
import com.inuker.bluetooth.bean.DataBean;
import com.inuker.bluetooth.library.connect.listener.BleConnectStatusListener;
import com.inuker.bluetooth.library.connect.listener.BluetoothStateListener;
import com.inuker.bluetooth.library.connect.response.BleConnectResponse;
import com.inuker.bluetooth.library.connect.response.BleNotifyResponse;
import com.inuker.bluetooth.library.connect.response.BleReadResponse;
import com.inuker.bluetooth.library.connect.response.BleWriteResponse;
import com.inuker.bluetooth.library.model.BleGattCharacter;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.inuker.bluetooth.library.utils.ByteUtils;
import com.ioon.CustomCallback;
import com.ioon.HeartRateMonitoring;
import com.zhuoting.health.Config;
import com.zhuoting.health.bean.BloodInfo;
import com.zhuoting.health.bean.ClockInfo;
import com.zhuoting.health.bean.HeartInfo;
import com.zhuoting.health.bean.SleepInfo;
import com.zhuoting.health.bean.SportInfo;
import com.zhuoting.health.notify.IDataResponse;
import com.zhuoting.health.notify.IErrorCommand;
import com.zhuoting.health.notify.IRequestResponse;
import com.zhuoting.health.parser.DataParser;
import com.zhuoting.health.parser.IOperation;
import com.zhuoting.health.util.DataUtil;
import com.zhuoting.health.util.Tools;
import com.zhuoting.health.util.TransUtils;
import com.zhuoting.health.write.ProtocolWriter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.inuker.bluetooth.library.Constants.REQUEST_SUCCESS;

/**
 * Created by Hqs on 2018/1/16
 */
public class MyBleService extends Service {

    CustomCallback myCallback;

    public int getResultadoFrecuencia() {
        return resultadoFrecuencia;
    }

    public void setResultadoFrecuencia(int resultadoFrecuencia) {
        this.resultadoFrecuencia = resultadoFrecuencia;
    }

    int resultadoFrecuencia;

    UUID serviceUUID;
    private String mMac ;
    public static MyBleService myBleService ;
    public boolean isConnected = false;
    int datalenght = 0;
    byte[] databyte;
    @Override
    public void onCreate() {
        super.onCreate();
        myBleService = this ;
        ClientManager.getClient().registerBluetoothStateListener(bluetoothStateListener);
        DataParser.newInstance().setDataResponseListener(iDataResponse);
        DataParser.newInstance().setRequestResponseListener(iRequestResponse);
        DataParser.newInstance().setErrorCommandListener(iErrorCommand);
        DataParser.newInstance().setOperation(iOperation);
    }

    public static MyBleService getInstance(){
        return myBleService ;
    }

    public IDataResponse getiDataResponse() {
        return iDataResponse;
    }

    public boolean isConnect(){
        if( mMac == null ){
            return false ;
        }
        int connectStatus = ClientManager.getClient().getConnectStatus(mMac);
        if( connectStatus == BluetoothProfile.STATE_CONNECTED ){
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private BluetoothStateListener bluetoothStateListener = new BluetoothStateListener() {
        @Override
        public void onBluetoothStateChanged(boolean openOrClosed) {

        }
    };

    /**
     * 蓝牙连接
     * @param mac       mac地址
     */
    public void connect(String mac){
        Log.d("tttt","我在连接");
        mMac = mac ;
        ClientManager.getClient().connect(mac,bleConnectResponse);
        ClientManager.getClient().registerConnectStatusListener(mac,connectStatusListener);
    }

    /**
     * 蓝牙断开
     */

    public void disConnect(String mac){
        ClientManager.getClient().disconnect(mac);
    }


    private BleConnectResponse bleConnectResponse = new BleConnectResponse() {
        @Override
        public void onResponse(int code, BleGattProfile data) {
            Log.e("connectRes","code = "+code);
                if( code == 0 ){        // 0 成功
                    isConnected = true;
                    SPUtils.saveBleAddress(mMac,MyBleService.this);
                    setGattProfile(data);
                }else{
                    isConnected = false;
                }
        }
    };

    private BleConnectStatusListener connectStatusListener = new BleConnectStatusListener() {
        @Override
        public void onConnectStatusChanged(String mac, int status) {
            Log.e("connectStatus",status+"");
                if( status == 0x10){
                    isConnected = true;
                    EventBus.getDefault().post(new ConnectBean(isConnected));
                }else{
                    isConnected = false;
                    EventBus.getDefault().post(new ConnectBean(isConnected));
                }
        }
    };

    public void setGattProfile(BleGattProfile profile) {
        List<String> items = new ArrayList<String>();
        List<com.inuker.bluetooth.library.model.BleGattService> services = profile.getServices();
        for (com.inuker.bluetooth.library.model.BleGattService service : services) {
//            Log.e("uuid","service : "+service.getUUID().toString());
            if(Config.char0.equalsIgnoreCase(service.getUUID().toString())){
                serviceUUID = service.getUUID();
                List<BleGattCharacter> characters = service.getCharacters();
                for(BleGattCharacter character : characters){
//                    String uuidCharacteristic = character.getUuid().toString();
//                    Log.e("uuid","characteristic : "+uuidCharacteristic);
                    if( character.getUuid().toString().equalsIgnoreCase(Config.char1)){     // 主要用于回复等操作 Se utiliza principalmente para responder y otras operaciones.
                        Log.e("IctusAPP", "--------------------------------------- Respuesta en Config.char1");

                        openid(serviceUUID,character.getUuid());
                    }else if(character.getUuid().toString().equalsIgnoreCase(Config.char3)){    // 主要用于实时数据、批量数据上传 Se utiliza principalmente para datos en tiempo real, carga masiva de datos
                        Log.e("IctusAPP", "--------------------------------------- Respuesta en Config.char3");

                        openid(serviceUUID,character.getUuid());
                    }
                }
            }
        }
    }


    public void openid(UUID serviceUUID, UUID characterUUID) {
        Log.e("IctusAPP", "--------------------------------------- Valores recibidos de bleNotifyResponse: serviceUUID:" + serviceUUID + " / characterUUID:" + characterUUID );
        ClientManager.getClient().indicate(mMac,serviceUUID,characterUUID,bleNotifyResponse);
    }

    private BleNotifyResponse bleNotifyResponse = new BleNotifyResponse() {
        @Override
        public void onNotify(UUID service, UUID character, byte[] value) {
            String data = DataUtil.byteToHexString(value);
             Log.e("IctusAPP", "--------------------------------------- DATAPARSER array notify: " + value);

             Log.e("IctusAPP", "--------------------------------------- DATAPARSER 0: " + Arrays.toString(value));
             Log.e("IctusAPP", "--------------------------------------- onNotifyDATAPARSER 1: " + data);


            if( data.length() <= 100){
                //Log.e("onNotify",character.toString()+"\n"+data);
                EventBus.getDefault().post(new DataBean(data));
                // Log.e("onNotify",Tools.logbyte(value));
                 Log.e("IctusAPP", "--------------------------------------- if( data.length() <= 100){  DATAPARSER 2.1" + Tools.logbyte(value));

            }else {
                EventBus.getDefault().post(new DataBean(data));
                 Log.e("IctusAPP", "--------------------------------------- else DATAPARSER 2.2: " + Tools.logbyte(value));

            }

            if (datalenght == 0) {
                byte[] countl = {0x00, 0x00, value[3], value[2]};
                datalenght = TransUtils.Bytes2Dec(countl);
                databyte = value;
            }else{
                databyte = Tools.byteMerger(databyte,value);
            }

            if (datalenght==databyte.length){
                // Log.e("IctusAPP", "--------------------------------------- DATAPARSER 3: " + Arrays.toString(databyte));
                DataParser.newInstance().parseData(databyte);
                datalenght =0;
            }
        }

        @Override
        public void onResponse(int code) {
            Log.d("chen",code+"");
        }
    };


    public IRequestResponse iRequestResponse = new IRequestResponse() {

        /**
         * 运动模式
         * @param result          0 成功       1 失败
         */
        @Override
        public void onsetSportTypeResponse(byte result){
            Log.e("dataRes","onsetSportTypeResponse "+result);
        }

        @Override
        public void onFirmWareUpdateResponse(byte result) {
            Log.e("result","onFirmWareUpdateResponse "+result);
        }

        @Override
        public void onDeleteDownloadedFirmWare(byte result) {
            Log.e("result","onDeleteDownloadedFirmWare "+result);
        }

        @Override
        public void onUpdateFWStatusResponse(byte result) {
            Log.e("result","onUpdateFWStatusResponse "+result);
        }

        @Override
        public void onFirmWareBlockResponse(byte result) {
            Log.e("result","onFirmWareBlockResponse "+result);
        }

        @Override
        public void onTimeSettingResponse(byte result) {
            Log.e("result","onTimeSettingResponse "+result);
        }

        @Override
        public void onAlarmSettingResponse(byte result) {
            Log.e("result","onAlarmSettingResponse "+result);
        }

        @Override
        public void onDeleteAlarmSetting(byte result) {
            Log.e("result","onDeleteAlarmSetting "+result);
        }

        @Override
        public void onModifyAlarmResponse(byte result) {
            Log.e("result","onModifyAlarmResponse "+result);
        }

        @Override
        public void onTargetSettingResponse(byte result) {
            Log.e("result","onTargetSettingResponse "+result);
        }

        @Override
        public void onUserInfoSettingResponse(byte result) {
            Log.e("result","onUserInfoSettingResponse "+result);
        }

        @Override
        public void onUnitSettingResponse(byte result) {
            Log.e("result","onUnitSettingResponse "+result);
        }

        @Override
        public void onLongsitSettingResponse(byte result) {
            Log.e("result","onLongsitSettingResponse "+result);
        }

        @Override
        public void onPreventLostOnOffResponse(byte result) {
            Log.e("result","onPreventLostOnOffResponse "+result);
        }

        @Override
        public void onPreventLostParamSettingResponse(byte result) {
            Log.e("result","onPreventLostParamSettingResponse "+result);
        }

        @Override
        public void onLeftOrRightHandSettingResponse(byte result) {
            Log.e("result","onLeftOrRightHandSettingResponse "+result);
        }

        @Override
        public void onMobileOSSettingResponse(byte result) {
            Log.e("result","onMobileOSSettingResponse "+result);
        }

        @Override
        public void onNotificationSettingResponse(byte result) {
            Log.e("result","onNotificationSettingResponse "+result);
        }

        @Override
        public void onHeartRateAlarmSettingResponse(byte result) {
            Log.e("result","onHeartRateAlarmSettingResponse "+result);
        }

        @Override
        public void onHeartRateMonitorResponse(byte result) {
            Log.e("result","onHeartRateMonitorResponse "+result);
        }

        @Override
        public void onFindMobileOnOffResponse(byte result) {
            Log.e("result","onFindMobileOnOffResponse "+result);
        }

        @Override
        public void onRecoverToDefaultSettingResponse(byte result) {
            Log.e("result","onRecoverToDefaultSettingResponse "+result);
        }

        @Override
        public void onDisturbeSettingResponse(byte result) {
            Log.e("result","onDisturbeSettingResponse "+result);
        }

        @Override
        public void onAerobicExerciseResponse(byte result) {
            Log.e("result","onAerobicExerciseResponse "+result);
        }

        @Override
        public void onLanguageSettingResponse(byte result) {
            Log.e("result","onLanguageSettingResponse "+result);
        }

        @Override
        public void onLeftTheWristToBrightResponse(byte result) {
            Log.e("result","onLeftTheWristToBrightResponse "+result);
        }

        @Override
        public void onBrightnessSettingResponse(byte result) {
            Log.e("result","onBrightnessSettingResponse "+result);
        }

        @Override
        public void onFindBandResponse(byte result) {
            Log.e("result","onFindBandResponse "+result);
        }

        @Override
        public void onHRMeasurementOnoffControl(byte result) {
            Log.e("result","onHRMeasurementOnoffControl "+result);
            Log.e("result","---------------------------- Resutlado del onHRMeasurementOnoffControl: Convertido-" + new String(String.valueOf(result)) + " / Normal-" + result);
            Toast.makeText(getApplicationContext(), "---------------------------- Resutlado del onHRMeasurementOnoffControl: " + new String(String.valueOf(result)), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onBPMeasurementOnoffControl(byte result) {
            Log.e("result","onBPMeasurementOnoffControl "+result);
        }


        @Override
        public void onBloodPressureCalibration(byte result) {
            Log.e("result","onBloodPressureCalibration "+result);
        }

        @Override
        public void onAppExitResponse(byte result) {
            Log.e("result","onAppExitResponse "+result);
        }

        @Override
        public void onAerobicExerciseOnOffResponse(byte result) {
            Log.e("result","onAerobicExerciseOnOffResponse "+result);
        }

        @Override
        public void onBindDeviceResponse(byte result) {
            Log.e("result","onBindDeviceResponse "+result);
        }

        @Override
        public void onUnBindDeviceResponse(byte result) {
            Log.e("result","onUnBindDeviceResponse "+result);
        }

        @Override
        public void onMessageNotificationResponse(byte result) {
            Log.e("result","onMessageNotificationResponse "+result);
        }

        @Override
        public void onRealTimeDataResponse(byte result) {
            Log.e("result","onRealTimeDataResponse "+result);
        }

        @Override
        public void onWaveFormPostResponse(byte result) {
            Log.e("result","onWaveFormPostResponse "+result);
        }

        @Override
        public void onFindPhoneResponse(byte result) {
            Log.e("result","onFindPhoneResponse "+result);
        }

        @Override
        public void onPreventLostResponse(byte result) {
            Log.e("result","onPreventLostResponse "+result);
        }

        @Override
        public void onAnswerOrRejectPhoneResponse(byte result) {
            Log.e("result","onAnswerOrRejectPhoneResponse "+result);
        }

        @Override
        public void onControlTheCamera(byte result) {
            Log.e("result","onControlTheCamera "+result);
        }

        @Override
        public void onControlTheMusic(byte result) {
            Log.e("result","onControlTheMusic "+result);
        }

        @Override
        public void onSynchronizdAllSwitchResponse(byte result) {
            Log.e("result","onSynchronizdAllSwitchResponse "+result);
        }

        @Override
        public void onBlockConfirmResponse(byte result) {
            Log.e("result","onBlockConfirmResponse "+result);
        }

        @Override
        public void onDeleteSportData(byte result) {
            Log.e("result","onDeleteSportData "+result);
        }

        @Override
        public void onDeleteSleepData(byte result) {
            Log.e("result","onDeleteSleepData "+result);
        }

        @Override
        public void onDeleteHeartRateData(byte result) {
            Log.e("result","onDeleteHeartRateData "+result);
        }

        @Override
        public void onDeleteBloodPressureData(byte result) {
            Log.e("result","onDeleteBloodPressureData "+result);
        }

        @Override
        public void onTestBlood(byte result) {
            Log.e("result","onTestBlood "+result);
            //write(ProtocolWriter.writeForUpdateWaveData());
        }

        @Override
        public void onUpdateWaveData(byte result) {
            Log.e("result","onUpdateWaveData "+result);
//            try {
//                Thread.sleep(100);
//                write(ProtocolWriter.writeForSamplingRate());
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }

        @Override
        public void onSamplingRate(byte result1,byte result2) {
            Log.e("result","onSamplingRate "+result1+":"+result2);
        }

        @Override
        public void onUpdateOptoelectronicWaveform(byte[] result) {
            perGcdMsg(result);
//            Log.e("result","onUpdateOptoelectronicWaveform "+result);
        }


        /**
         * ppg
         * @param result          0 成功       1 失败
         */
        @Override
        public void onsetPPGHZResponse(byte result){
            Log.i("IctusAPP", "---------------------- Recibiendo datos de onsetPPGHZResponse: Respuesta" + result);
            write(ProtocolWriter.writeForWaveUploadControl((byte)1,(byte)0));
        }

        @Override
        public void onSetSkinColor(byte result) {
            Log.e("result","onSetSkinColor:"+result);
        }

        @Override
        public void onBloodOxygenMeasure(byte result) {
            Log.e("dataRes","result:"+result);
        }

        @Override
        public void onRespiratoryRateMeasure(byte result) {
            Log.e("dataRes","result:"+result);
        }

        @Override
        public void onWeatherResponse(byte result) {
            Log.e("dataRes","result:"+result);
        }

        @Override
        public void onBloodPressureResponse(byte result) {
            Log.e("dataRes","result:"+result);
        }
    };

    public void perGcdMsg(byte[] readData){

        int lenght = readData.length - 6;

        int headCount = 4;

        int allCount = lenght/3;

        for (int i= 0; i<allCount ;i++) {

            int val = 0;
            int newHex = (readData[headCount+2+i*3] & 0xff);
            String newHexStr = Integer.toBinaryString(newHex);

            int index = 8 - newHexStr.length();
            for (int x=0;x<index;x++){
                newHexStr = "0"+newHexStr;
            }
//            System.out.println("xxxx="+newHexStr);
            String erStr = "0";
            if (newHexStr.length() == 8) {
                erStr.substring(0,1);
            }

            if (erStr.equals("1")) {
                byte[] bval = { (byte) 0xff ,readData[headCount+i*3+2],readData[headCount+i*3+1],readData[headCount+i*3]};
                val = TransUtils.Bytes2Dec(bval);
            }else{
                byte[] bval = { (byte) 0x00 ,readData[headCount+i*3+2],readData[headCount+i*3+1],readData[headCount+i*3]};
                val = TransUtils.Bytes2Dec(bval);
            }

            Log.e("result","onDeleteBloodPressureData :"+val);
        }
    }

    IDataResponse iDataResponse = new IDataResponse() {
        @Override
        public void onFirmWareInfoResponse(byte argument, int id, byte status, byte electricity, byte crfwsubversion, byte crfwmainversion, byte dldfwsubversion, byte dlfwmainversion, int dlbytes) {
            Log.e("dataRes","onFirmWareInfoResponse : argument:"+argument+" id:"+id+" electricity: "+electricity+
                    " crfwsubversion: "+crfwsubversion+" crfwmainversion: "+crfwmainversion+" dldfwsubversion: "+dldfwsubversion+" dlfwmainversion: "+dlfwmainversion
            +" dlbytes: "+dlbytes);
        }

        @Override
        public void onQueryAlarmClock(byte supportAlarmNum, byte settingAlarmNum, List<ClockInfo> alarmSet) {
            Log.e("dataRes","onQueryAlarmClock : "+"supportAlarmNum : "+supportAlarmNum+" settingAlarmNum : "+settingAlarmNum

            +"alarmSet:"+alarmSet.get(0).toString());
        }

        @Override
        public void onDeviceBaseInfo(int deviceId, int subVersion, int mainVersion, byte electricityStatus, byte electricity, byte bindStatus, byte synchronizedFlag) {
            Log.e("dataRes","onDeviceBaseInfo :  deviceId : "+deviceId+" subVersion : "+subVersion+" mainVersion : "+mainVersion+" electricityStatus : "+electricityStatus
            +" electricity : "+electricity+" bindStatus: "+bindStatus+" synchronizedFlag: "+synchronizedFlag);
        }

        @Override
        public void onDeviceSupportFunction(byte function1, byte function2, byte alarmNum, byte alarmType, byte messageNotify1, byte messageNotify2, byte otherFunction) {

        }

        @Override
        public void onDeviceMac(String mac) {

            Log.e("dataRes","onDeviceMac : "+mac);
        }

        @Override
        public void onDeviceName(String deviceName) {
            Log.e("dataRes","onDeviceName : "+deviceName);
        }

        @Override
        public void onCurrentHR(byte status, int hr) {
            Log.e("dataRes","onCurrentHR : status : "+status+" hr : "+hr);
        }

        @Override
        public void onCurrentBP(byte status, int systolic, int diastolic) {
            //setBPValue(diastolic, diastolic, 1);                                             /****************************************************************/
            Log.e("dataRes","onCurrentBP : status : "+status+" systolic : "+systolic+" diastolic : "+diastolic);
        }

        @Override
        public void onQuerySamplingFreqResponse(int frequency) {
            Log.e("dataRes","onQuerySamplingFreqResponse : "+frequency);
        }

        @Override
        public void onSynchronizedTodaySport(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedTodaySport   recordNum:"+recordNum+" packDataLength:"+packDataLength+" allDataLength: "+allDataLength);
        }

        @Override
        public void onTodaySport(List<SportInfo> mlist) {
            for (SportInfo sportInfo : mlist) {
                Log.e("dataRes", "onTodaySport " + sportInfo.toString());
            }
        }

        @Override
        public void onSynchronizedHistorySport(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedHistorySport  recordNum : "+recordNum+" packDataLength : "+packDataLength
            +" allDataLength : "+allDataLength);
        }

        @Override
        public void onHistorySport(List<SportInfo> mlist) {
            for (SportInfo sportInfo : mlist) {
                Log.e("dataRes", "onHistorySport " + sportInfo.toString());
            }
        }

        @Override
        public void onSynchronizedTodaySleep(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedTodaySleep recordNum :"+recordNum+" packDataLength : "+packDataLength
            +" allDataLength: "+allDataLength);
        }

        @Override
        public void onTodaySleep(SleepInfo sleepInfo) {
            Log.e("dataRes","onTodaySleep "+sleepInfo.toString());
        }

        @Override
        public void onSynchronizedHistorySleep(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedHistorySleep recordNum : "+recordNum+" packDataLength : "+packDataLength+" allDataLength: "+allDataLength);
        }

        @Override
        public void onHistorySleep(List<SleepInfo> mlist) {
            for (SleepInfo sleepInfo : mlist) {
                Log.e("dataRes", "onHistorySleep " + sleepInfo.toString());
                Toast.makeText(MyBleService.getInstance(),sleepInfo.toString(),Toast.LENGTH_SHORT).show();
            }
            Log.e("dataRes", "onHistorySleep " + mlist.size());
        }

        @Override
        public void onSynchronizedTodayHeartRate(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedTodayHeartRate recordNum:"+recordNum+" packDataLength : "+packDataLength+" allDataLength : "+allDataLength);
        }

        @Override
        public void onTodayHeartRate(List<HeartInfo>  mlist) {
            for (HeartInfo heartInfo : mlist) {
                Log.e("dataRes", "onTodayHeartRate " + heartInfo.toString());
            }
        }

        @Override
        public void onSynchronizedHistoryHeartRate(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedHistoryHeartRate recordNum : "+recordNum+" packDataLength : "+packDataLength+" allDataLength : "+allDataLength);
        }

        @Override
        public void onHistoryHeartRate(List<HeartInfo>  mlist) {
            for (HeartInfo heartInfo : mlist) {
                Log.e("dataRes", "onHistoryHeartRate " + heartInfo.toString());

            }
        }

        @Override
        public void onSynchronizedTodayBloodPressure(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedTodayBloodPressure recordNum : "+recordNum+" packDataLength : "+packDataLength+" allDataLength : "+allDataLength);
        }

        @Override
        public void onTodayBloodPressure(List<BloodInfo>  mlist) {
            for (BloodInfo bloodInfo : mlist) {
                Log.e("dataRes", "onTodayBloodPressure " + bloodInfo.toString());
            }
        }

        @Override
        public void onSynchronizedHistoryBloodPressure(int recordNum, int packDataLength, int allDataLength) {
            Log.e("dataRes","onSynchronizedHistoryBloodPressure recordNum : "+recordNum+" packDataLength : "+packDataLength+" allDataLength : "+allDataLength);
        }

        @Override
        public void onHistoryBloodPressure(List<BloodInfo>  mlist) {
            for (BloodInfo bloodInfo : mlist){
                Log.e("dataRes", "onHistoryBloodPressure " + bloodInfo.toString());
            }
        }

        @Override
        public void onRealTimeSportData(int steps, int distance, int calorie) {
            Log.e("result","-------------------------------- Datos de onRealTimeSportData steps : "+steps+" distance : "+distance+" calorie : "+calorie);
            Log.e("dataRes","onRealTimeSportData steps : "+steps+" distance : "+distance+" calorie : "+calorie);
        }


        @Override
        public void onRealTimeHeartRate(int heartRate) {
            setHRValue(heartRate);
            Log.e("IctusAPP","onRealTimeHeartRate heartRate : "+heartRate);

        }


        @Override
        public void onRealTimeOxygen(int oxygen) {
            Log.e("dataRes","onRealTimeOxygen oxygen : "+oxygen);
        }

        @Override
        public void onRealTimeBloodPressure(int systolic, int diastolic, int heartRate) {
            setBPValue(systolic, diastolic, heartRate);
            HeartRateMonitoring heartRateMonitoring = new HeartRateMonitoring(getApplicationContext());
            heartRateMonitoring.writeFile("MyHeartData.dat", "s:"+systolic+".d:"+diastolic+".h:"+heartRate);
            Log.e("dataRes","---------------------------------------------------------------- onRealTimeBloodPressure systolic : "+systolic+" diastolic : "+diastolic+" heartRate : "+heartRate);
        }


        @Override
        public void onElectrocardiogram(int  Ecg_val) {
//            Log.e("dataRes","onElectrocardiogram"+DataUtil.byteToHexString(electrocardiogram).substring(0,50));
        }

        @Override
        public void onOptoelectronic(byte[] optoelectronic) {
            Log.e("IctusAPP","-------------------------------------- Resultado de onElectrocardiogram: " + Arrays.toString(optoelectronic));
            Log.e("dataRes","onOptoelectronic "+DataUtil.byteToHexString(optoelectronic).substring(0,50));
        }

        @Override
        public void onSportMode(int steps, int instance, int kacl, int sportTime) {
            Log.e("dataRes","steps:"+steps+",instance"+instance+",kacl"+kacl+",sportTime"+sportTime);
        }


    };

    int systolic, diastolic, heartRate = 0;
    int valueBP = 0;

    public void setHRValue(int value){
        heartRate = value;
    }

    public int getHRValue(){
        return heartRate;
    }


    public void setBPValue(int systolic, int diastolic, int heartRate){
        this.systolic = systolic;
        this.diastolic = diastolic;
        this.heartRate = heartRate;
    }

    public int getBPValue(){
        return valueBP;
    }

    IErrorCommand iErrorCommand = new IErrorCommand() {
        @Override
        public void onErrorCommand(String commandIdAndKey, int errorType) {
            Log.e("errorCommand","commandIdAndKey : "+commandIdAndKey+" errorType : "+errorType);
        }
    };

    IOperation iOperation = new IOperation() {
        @Override
        public void onDoSynchronizedHistorySport() {
            byte[] smsg = {0x05, 0x02, 0x01};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDoSynchronizedHistorySleep() {
            byte[] smsg = {0x05, 0x04, 0x01};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDoSynchronizedHistoryHeartRate() {
            byte[] smsg = {0x05, 0x06, 0x01};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDoSynchronizedHistoryBloodPressure() {
            byte[] smsg = {0x05, 0x08, 0x01};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDeleteSport() {
            byte[] smsg = {0x05, 0x40, 0x02};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDeleteSleep() {
            byte[] smsg = {0x05, 0x41, 0x02};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDeleteHeartRate() {
            byte[] smsg = {0x05, 0x42, 0x02};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDeleteBloodPressure() {
            byte[] smsg = {0x05, 0x43, 0x02};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }

        @Override
        public void onDoReceiveAllComplete() {
            byte[] smsg = {0x05, (byte) 0x80, 0x01};
            smsg = Tools.makeSend(smsg);
            write(smsg);
        }
    };

    /**
     *  写数据
     * @param data      写入蓝牙的数据
     */
    public void write(byte[] data){
        Log.d("chen888",Tools.logbyte(data));
        Log.d("IctusApp","------------------------------ Log de METODO WRITE: data: " + Arrays.toString(data));
        Log.e("IctusApp","------------------------------ Log de METODO WRITE: UUID.fromString(Config.char1): " + UUID.fromString(Config.char1));
        Log.e("IctusApp","------------------------------ Log de METODO WRITE: serviceUUID:" +serviceUUID);

        ClientManager.getClient().write(mMac,serviceUUID,UUID.fromString(Config.char1),data,writeResponse);
        Log.i("IctusAPP", "El primer write: ");

    }

    private BleWriteResponse writeResponse = new BleWriteResponse() {

        @Override
        public void onResponse(int code) {
            Log.e("IctusApp","--------------Numero de Respuesta obtenida de la peticion = " + code);
        }
    };

    public void writeWithoutResponse(byte[] data){
        ClientManager.getClient().writeNoRsp(mMac,serviceUUID,UUID.fromString(Config.char2),data,writeResponse);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}
