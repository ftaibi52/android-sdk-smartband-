package com.ioon;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.inuker.bluetooth.MyBleService;
import com.inuker.bluetooth.R;
import com.zhuoting.health.bean.BloodInfo;
import com.zhuoting.health.bean.HeartInfo;
import com.zhuoting.health.parser.DataParser;
import com.zhuoting.health.write.ProtocolWriter;

import java.util.List;

public class Salud extends Activity {

    Button btnHistoricHealth;
    Button btnHistoricHealthBP;
    MyBleService device;
    TextView dataHealth;
    List<HeartInfo> arrayListHistoricHealth;
    List<BloodInfo> arrayListHistoricBlood;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_health);
        btnHistoricHealth = (Button) findViewById(R.id.btnHealth);
        btnHistoricHealthBP = (Button) findViewById(R.id.btnHealthBP);

        dataHealth = (TextView) findViewById(R.id.dataHealth);
        device = DeviceActions.getConectionDevice();

        device.write(ProtocolWriter.writeForSychronHistoryHRData((byte) 1));
        arrayListHistoricHealth = DataParser.getHeartlist();
        Log.i("MyApp-HistoricHealth", arrayListHistoricHealth.toString());

        device.write(ProtocolWriter.writeForSychronHistoryBPData((byte) 1));
        arrayListHistoricBlood = DataParser.getBloodlist();
        Log.i("MyApp-HistoricBloodP", arrayListHistoricBlood.toString());


        btnHistoricHealth.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                if(!arrayListHistoricHealth.isEmpty()){
                    dataHealth.setText(arrayListHistoricHealth.toString());

                } else {
                    dataHealth.setText("No hay datos de pulsaciones");
                }
            }
        });

        btnHistoricHealthBP.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                if(!arrayListHistoricBlood.isEmpty()){
                    dataHealth.setText(arrayListHistoricBlood.toString());

                } else {
                    dataHealth.setText("No hay datos de presión sanguínea");
                }
            }
        });
    }
}
