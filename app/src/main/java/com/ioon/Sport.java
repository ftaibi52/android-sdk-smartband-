package com.ioon;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.inuker.bluetooth.MyBleService;
import com.inuker.bluetooth.R;
import com.zhuoting.health.bean.SleepInfo;
import com.zhuoting.health.bean.SportInfo;
import com.zhuoting.health.parser.DataParser;
import com.zhuoting.health.write.ProtocolWriter;

import java.util.List;

public class Sport extends AppCompatActivity {

    Button btnSportHistoric;
    MyBleService device;
    TextView data_sport;
    List<SportInfo> arrayListHistoricSport;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sport);
        arrayListHistoricSport = DataParser.getSportlist() ;
        btnSportHistoric = (Button) findViewById(R.id.btnSport);
        data_sport = (TextView) findViewById(R.id.data_sport);
        device = DeviceActions.getConectionDevice();

        device.write(ProtocolWriter.writeForSychronHistorySportData((byte) 1));
        arrayListHistoricSport = DataParser.getSportlist();
        Log.i("MyApp-Historic Sport", arrayListHistoricSport.toString());

        btnSportHistoric.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                if(!arrayListHistoricSport.isEmpty()){
                    data_sport.setText(arrayListHistoricSport.toString());

                } else {
                    data_sport.setText("No hay datos");
                }
            }
        });


    }
}
