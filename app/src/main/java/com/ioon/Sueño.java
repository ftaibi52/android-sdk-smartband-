package com.ioon;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.inuker.bluetooth.MyBleService;
import com.inuker.bluetooth.R;
import com.zhuoting.health.bean.SleepInfo;
import com.zhuoting.health.bean.SleepMegInfo;
import com.zhuoting.health.parser.DataParser;
import com.zhuoting.health.write.ProtocolWriter;

import java.util.ArrayList;
import java.util.List;



public class Sueño extends AppCompatActivity {

    Button btnSleepHistoric;
    MyBleService device;
    TextView datos_sueño;
    List<SleepInfo> arrayListHistoricSleep;
    private int deepSleepTimes;
    private int lightSleepCount;
    private int totalDeepSleepTime;
    private int totalLightSleepTime;
    private String date;
    private String startSleepTime;
    private String endSleepTime;
    private LinearLayout linerLayout;
    private ScrollView scroll;
    private int i;



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sleep);

        linerLayout = (LinearLayout) findViewById(R.id.linearLayout);
        scroll = (ScrollView) findViewById(R.id.scroll);
        arrayListHistoricSleep = DataParser.getSleeplist();
        btnSleepHistoric = (Button) findViewById(R.id.btnSueño);
        datos_sueño = (TextView) findViewById(R.id.datos_sueño);
        device = DeviceActions.getConectionDevice();

        device.write(ProtocolWriter.writeForSychronHistorySleepData((byte) 1));
        arrayListHistoricSleep = DataParser.getSleeplist();
        Log.i("MyApp-HistoricSleep", arrayListHistoricSleep.toString());


        btnSleepHistoric.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View view) {

                if(!arrayListHistoricSleep.isEmpty()){
                    datos_sueño.setText(arrayListHistoricSleep.toString());

                } else {
                    datos_sueño.setText("No hay datos");
                }


            }
        });

//        LineChart chart = (LineChart) findViewById(R.id.chart);
//
//        List<Entry> entries = new ArrayList<>();
//
//        for( SleepInfo sl: arrayListHistoricSleep) {
//
//            entries.add(new Entry(sl.dsTimes, sl.dsCount));
//            for (SleepMegInfo sl_info : sl.mlist){
//
//                long stime = sl_info.stime;
//
//            }
//
//        }
//
//        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
//
//        LineData lineData = new LineData(dataSet);
//        chart.setData(lineData);
//        chart.invalidate(); // refresh

        BarChart chart = (BarChart) findViewById(R.id.chartBar);

        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0f, 30f));
        entries.add(new BarEntry(1f, 80f));
        entries.add(new BarEntry(2f, 60f));
        entries.add(new BarEntry(3f, 50f));
        // gap of 2f
        entries.add(new BarEntry(5f, 70f));
        entries.add(new BarEntry(6f, 60f));
        BarDataSet set = new BarDataSet(entries, "BarDataSet");

        BarData data = new BarData(set);
        data.setBarWidth(0.9f); // set custom bar width
        chart.setData(data);
        chart.setFitBars(true); // make the x-axis fit exactly all bars
        chart.invalidate(); // refresh


    }

}
