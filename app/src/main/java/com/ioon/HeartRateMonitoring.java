package com.ioon;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.MyBleService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class HeartRateMonitoring implements Runnable {


    private Context context;
    boolean reading = false;
    MyBleService conectionDevice;
    int value;

    public HeartRateMonitoring(MyBleService conectionDevice) {
        this.conectionDevice = conectionDevice;

    }

    public HeartRateMonitoring(Context context) {
        this.context = context;

    }

    public void writeFile(String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "ictus");
            if (!root.exists()) {
                root.mkdirs();
            }
            File gpxfile = new File(root, sFileName);
            FileWriter fileWriter = new FileWriter(gpxfile);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.append(sBody);
            bufferedWriter.newLine();
            bufferedWriter.close();
            fileWriter.close();
            Toast.makeText(context, "Guardado", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {

        reading = true;

        do{

            value = conectionDevice.getResultadoFrecuencia();
            Log.i("IctusAPP", "########################################## Hilo: " + value);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }while(reading);


    }


    public void stop(){
        reading = false;
    }

    public void iniciar(){
        reading = true;
    }

    public int getValue() {
        return value;
    }

    public void updateValue(CustomCallback customCallback){

        customCallback.myResponse(value);

    }

}
