package com.ioon;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.ClientManager;
import com.inuker.bluetooth.MyBleService;
import com.inuker.bluetooth.R;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.zhuoting.health.MyApplication;
import com.zhuoting.health.parser.DataParser;
import com.zhuoting.health.write.ProtocolWriter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static java.lang.System.err;

public class DeviceActions extends AppCompatActivity {

    TextView txtDeviceName;
    TextView txtResult;
    TextView txtDiastolic;
    TextView txtSistolic;
    TextView txtGiro;
    Button btnPulsaciones;
    Button btnStop;
    Button btnConvert;
    Button btnDistancia;
    Button btn_sueño;
    Button btnSport;
    Button btnHealth;

    FrameLayout layoutVistaResultados;

    String mac;

    static MyBleService conectionDevice;
    SearchResult deviceSelected;

    boolean reading = false;
    //HeartRateMonitoring heartRateMonitoring;
    boolean start = false;
    ReadingOperation readingOperation;


    public static MyBleService getConectionDevice() {
        return conectionDevice;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_actions);

        Bundle bundleExtras = getIntent().getExtras();

        txtSistolic = (TextView) findViewById(R.id.sistolic);
        txtDiastolic = (TextView) findViewById(R.id.distiolic);
        txtResult = (TextView) findViewById(R.id.state);

        if (bundleExtras != null) {
            deviceSelected = (SearchResult) bundleExtras.get("deviceSelected");
            mac = (String) bundleExtras.get("mac");
        }

        Log.i("IctusAPP", "############################################# Direccion MAC: " + mac);
        conectionDevice = MyBleService.getInstance();

        if(deviceSelected == null){
            conectionDevice.connect(mac);
            Toast.makeText(getApplicationContext(), "Dispositivo null", Toast.LENGTH_LONG).show();
            Log.i("IctusAPP", "Dispositivo null y conectando");
        }else{
            conectionDevice.connect(deviceSelected.getAddress());
            Toast.makeText(getApplicationContext(), "Dispositivo no null", Toast.LENGTH_LONG).show();
            Log.i("IctusAPP", "Dispositivo almacenado y conectando");

        }


        btn_sueño = (Button) findViewById(R.id.btn_sueño);
        Intent intent1 = new Intent(this, Sueño.class);

        btn_sueño.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent1);
            }
        });

        btnSport = (Button) findViewById(R.id.btnSport);
        Intent intent2 = new Intent(this, Sport.class);

        btnSport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent2);
            }
        });

        btnHealth = (Button) findViewById(R.id.btnSalud);
        Intent intent3 = new Intent(this, Salud.class);

        btnHealth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent3);
            }
        });

        //searchDevices();

        txtDeviceName = (TextView) findViewById(R.id.txtDeviceName);
        btnPulsaciones = (Button) findViewById(R.id.btnPulsaciones);
        btnStop = (Button) findViewById(R.id.btnStop);
        btnConvert = (Button) findViewById(R.id.btnConvert);

        layoutVistaResultados = (FrameLayout) findViewById(R.id.layoutVistaResultados);

        txtDeviceName.setText("Dispositivo " );
        DataParser dataParser = DataParser.newInstance();

        //heartRateMonitoring = new HeartRateMonitoring(conectionDevice);
        //Thread hiloHR = new Thread(new HeartRateMonitoring(conectionDevice));

        int i = MyApplication.getInstance().initHeart(250);
        readingOperation = new ReadingOperation(dataParser);

        btnPulsaciones.setOnClickListener(view -> {
            if(conectionDevice.isConnected){

                if(!start){

                    //conectionDevice.write(ProtocolWriter.writeForDataUpload((byte) 1, (byte) 1, (byte) 2)); //NADA
                    //hiloHR.start();
                    start = true;
                    reading = true;

                    txtDeviceName.setText("Leyendo...");
                    // El 1 mide solo una vez, el 2 se queda constantemente midiendo y el 0 para la medicion
                   // conectionDevice.write(ProtocolWriter.writeForBPMeasure ((byte) 2)); // OK. Mide la presion sanguinea, pero solo devuelve las pulsaciones
                    conectionDevice.write(ProtocolWriter.writeForHRMeasure ((byte) 2));
                    //conectionDevice.write(ProtocolWriter.writeForGetCurrentBP ()); //NADA

                   // conectionDevice.write(ProtocolWriter.writeForDataUpload((byte) 0x01, (byte) 0x02, (byte) 0)); // NADA


                    //conectionDevice.write(ProtocolWriter.writeForSychronHistorySleepData((byte) 0));
                    //conectionDevice.write(ProtocolWriter.writeForSychronHistorySleepData((byte) 1)); //OK. Obtiene historial de sueño

                   // conectionDevice.write(ProtocolWriter.writeForSychronHistorySportData((byte) 1));
                   // conectionDevice.write(ProtocolWriter.writeForGetSupportFunction());

                    // conectionDevice.write(ProtocolWriter.writeForSychronHistoryHRData((byte) 0));
                    // conectionDevice.write(ProtocolWriter.writeForSychronHistoryHRData((byte) 1));
                    // conectionDevice.write(ProtocolWriter.writeForAllDataOnOff((byte) 1)); NO funciona

                    //conectionDevice.write(ProtocolWriter.writeForDataUpload ((byte) 1, (byte) 3, (byte) 5));
                    //conectionDevice.write(ProtocolWriter.writeForWaveUploadControl ((byte) 1, (byte) 0));

                    //conectionDevice.write(ProtocolWriter.writeForGetCurrentBP()); //Funciona pero tienes que lanzar la medicion manual
                    //conectionDevice.write(ProtocolWriter.writeForHRMeasure((byte)2));

                    txtResult.setText("Valor: ");


                    if(readingOperation.isCancelled()){
                        readingOperation = new ReadingOperation(dataParser);
                        readingOperation.execute("");
                    }else{
                        txtDeviceName.setText("Finalizado");
                        readingOperation.execute("");
                    }

                }

            }else{
                txtDeviceName.setText("No estas conectado");
            }
        });


//
//        btnDistancia.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(conectionDevice.isConnect()){
//
//                    conectionDevice.write(ProtocolWriter.writeForBPMeasure((byte) 0));
//                    conectionDevice.write(ProtocolWriter.writeForDataUpload((byte) 1, (byte) 0, (byte) 3));
//                    //conectionDevice.write(ProtocolWriter.writeFor((byte) 0));
//                    //conectionDevice.write(ProtocolWriter.writeForDataUpload((byte) 1, (byte) 0, (byte) 3));
//                    txtGiro.setText("Valores giro: " + conectionDevice);
//
//                    //conectionDevice.write(ProtocolWriter.writeForHRMeasure ((byte) 2));
//
//                }
//            }
//        });
//
//        btnConvert.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("IctusApp", "---------------------------- Valor original: " + ProtocolWriter.writeForDataUpload((byte) 1, (byte) 0, (byte) 3) + " / Valor convertido: " + Arrays.toString(ProtocolWriter.writeForDataUpload((byte) 1, (byte) 0, (byte) 3)));
//                Log.i("IctusApp", "---------------------------- Valor individual: (byte) 1:" + (byte) 1 + (byte) 120);
//            }
//        });



        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                conectionDevice.write(ProtocolWriter.writeForBPMeasure ((byte) 0)); // OK. Para la medicion de la presion sanguinea

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ClientManager.getClient().disconnect(deviceSelected.getAddress());
    }


    public class ReadingOperation extends AsyncTask<String, Void, String> {

        int[] values;                       //systolic, diastolic, heart
        DataParser myBleService;
        List arraySleep;

        ReadingOperation(DataParser myBleService){
            this.myBleService = myBleService;
        }

        @Override
        protected String doInBackground(String... resultado) {
            //arraySleep = myBleService.getArraySleep();
            values = myBleService.getCurrentBPandHR();

            //Log.i("IctusAPP", "***************************************************** Leyendo: " + Arrays.toString(values));
            //Log.i("IctusAPP", "***************************************************** arraySleep: " + arraySleep);

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

            txtSistolic.setText("Ultimo valor Sistolica: " + values[0]);
            txtDiastolic.setText("Ultimo valor Diastolica: " + values[1]);

            readingOperation.selfRestart();
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}

        public void selfRestart() {
            readingOperation = new ReadingOperation(myBleService);
            readingOperation.execute("");
            //Log.i("IctusAPP", "***************************************************** Reiniciado");
        }

        public void stopReading(){
            readingOperation.cancel(true);
        }

    }

}


