package com.ioon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.ClientManager;
import com.inuker.bluetooth.MyBleService;
import com.inuker.bluetooth.R;
import com.inuker.bluetooth.SPUtils;
import com.inuker.bluetooth.library.beacon.Beacon;
import com.inuker.bluetooth.library.search.SearchRequest;
import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.search.response.SearchResponse;
import com.zhuoting.health.MyApplication;

import java.util.ArrayList;

public class ListDevices extends AppCompatActivity {

    ListView listView;
    Button btnBuscar;
    Button menu_tabs;
    TextView txtBusncar;
    private ClientManager clientManager;
    ArrayList arrayList;
    DetailsAdapter detailsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_devices);

        btnBuscar = (Button) findViewById(R.id.btnBuscar);
        int i = MyApplication.getInstance().initHeart(250);

        Intent intent = new Intent(this, MyBleService.class);
        startService(intent);

        //int i = MyApplication.getInstance().initHeart(250);
        //Log.e("initHeart"," i = "+i);
        arrayList = new ArrayList();
        txtBusncar = (TextView) findViewById(R.id.txtBusncar);
        listView = (ListView) findViewById(R.id.myList);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);


        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchDevices();
            }
        });


        try {
            String value = SPUtils.readBleAddress(this);
            if(value != null){
                Intent intentNew = new Intent(getApplicationContext(), DeviceActions.class);
                //intentNew.putExtra("deviceSelected", deviceSelected);
                intentNew.putExtra("mac", value);
                //intent.putExtra("connection", conectionDevice);
                //////////////////////////////////////////////////////startActivity(intentNew);
            }else{
                Toast.makeText(this, "No se ha recibido ningun valor", Toast.LENGTH_SHORT).show();

            }
        }catch (Exception error){
            Toast.makeText(this, "Se ha producido el error: " + error, Toast.LENGTH_SHORT).show();
        }

        detailsAdapter = new DetailsAdapter(getApplicationContext(), arrayList);
        listView.setAdapter(detailsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SearchResult deviceSelected = (SearchResult) adapterView.getItemAtPosition(i);

                //MyBleService conectionDevice = new MyBleService();

                //conectionDevice.connect(deviceSelected.getAddress());

                Intent intent = new Intent(getApplicationContext(), DeviceActions.class);
                intent.putExtra("deviceSelected", deviceSelected);
                //intent.putExtra("connection", conectionDevice);
                startActivity(intent);
            }
        });

    }


    public void searchDevices() {

        SearchRequest request = new SearchRequest.Builder().searchBluetoothLeDevice(5000, 2).build();

        ClientManager.getClient().search(request, searchResponse);

    }

    private final SearchResponse searchResponse = new SearchResponse() {
        @Override
        public void onSearchStarted() {
            txtBusncar.setText("Buscando...");
            arrayList.clear();
        }

        @Override
        public void onDeviceFounded(SearchResult device) {
            txtBusncar.setText("Buscando...");
            Beacon beacon = new Beacon(device.scanRecord);
            Log.i("IctusApp","------------- Dispositivo: " + device.getName());


            if (!arrayList.contains(device)&&(beacon.toString().contains("107803E8"))) {
                arrayList.add(device);
                detailsAdapter = new DetailsAdapter(getApplicationContext(), arrayList);
                listView.setAdapter(detailsAdapter);
                //notifyAll();

            }
        }

        @Override
        public void onSearchStopped() {
            txtBusncar.setText("Busqueda finalizada");

            detailsAdapter = new DetailsAdapter(getApplicationContext(), arrayList);
            listView.setAdapter(detailsAdapter);

        }

        @Override
        public void onSearchCanceled() {

        }

    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ClientManager.getClient().stopSearch();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ClientManager.getClient().stopSearch();
    }
}
