package com.ioon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inuker.bluetooth.R;
import com.inuker.bluetooth.library.search.SearchResult;

import java.util.List;

public class DetailsAdapter extends ArrayAdapter {

    private final List listData;
    Context context;

    DetailsAdapter(Context context, List listData) {
        super(context, R.layout.item_device_list, listData);
        this.context = context;
        this.listData = listData;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if(view == null){
            view = LayoutInflater.from(context).inflate(R.layout.item_device_list, parent, false);
        }

        SearchResult deviceSelected = (SearchResult) listData.get(position);

        TextView textView = (TextView) view.findViewById(R.id.txtItemTitle);
        TextView txtItemDescription = (TextView) view.findViewById(R.id.txtItemDescription);
        TextView txtItemNumber = (TextView) view.findViewById(R.id.txtItemNumber);

        textView.setText(deviceSelected.getName());
        txtItemDescription.setText(deviceSelected.device.describeContents() + "");
        txtItemNumber.setText(deviceSelected.device.getAddress());

        return view;

    }
}
